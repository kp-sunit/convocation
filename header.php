<?php
$pagename1 = end(explode('/', $_SERVER['REQUEST_URI']));
$pagename2 = explode('.', $pagename1);
$pagename = $pagename2[0];
?>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php" style="color:#167ce9"><i class="flaticon-university"></i> Leading <br><small>University</small></a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php echo ($pagename=='index' or $pagename=='')?'active':''; ?>"><a href="index.php" class="nav-link">Home</a></li>
          <li class="nav-item <?php echo $pagename=='about'?'active':''; ?>"><a href="about.php" class="nav-link">University</a></li>
          <li class="nav-item <?php echo $pagename=='course'?'active':''; ?>" style="display:none;"><a href="course.php" class="nav-link">Courses</a></li>
          <li class="nav-item <?php echo $pagename=='teacher'?'active':''; ?>"><a href="teacher.php" class="nav-link">Guests</a></li>
          <li class="nav-item <?php echo $pagename=='program'?'active':''; ?>"><a href="program.php" class="nav-link">Programs</a></li>
          <li class="nav-item <?php echo ($pagename=='registration' or $pagename=="registration_guideline")?'active':''; ?>"><a href="registration_guideline.php" class="nav-link">Registration</a></li>
          <li class="nav-item <?php echo $pagename=='event'?'active':''; ?>"><a href="event.php" class="nav-link">Previous Convocation</a></li>
          <li class="nav-item <?php echo $pagename=='contact'?'active':''; ?>"><a href="contact.php" class="nav-link">Contact</a></li>
          <li class="nav-item cta" style="display:none;"><a href="contact.php" class="nav-link"><span>Apply Now!</span></a></li>
        </ul>
      </div>
    </div>
  </nav>
