<?php
$setting=mysql_fetch_assoc(mysql_query("SELECT * FROM `convocation_settings` WHERE id=1"));
?>
<footer class="ftco-footer ftco-bg-dark ftco-section img" style="background-image: url(images/bg_2.jpg); background-attachment:fixed;">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-3">
            <div class="ftco-footer-widget mb-4">
              <h2><a class="navbar-brand" href="index.php" style="color:#167ce9"><i class="flaticon-university"></i> Leading <br/><br><small>University</small></a></h2>
              <p><?php echo $setting['footer_text'] ?>.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                  <li class="ftco-animate"><a href="<?php echo $setting['twitt_url'] ?>" target="_blank"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="<?php echo $setting['fb_url'] ?>"   target="_blank"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="<?php echo $setting['inst_url'] ?>"   target="_blank"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Previous Convocations</h2>
              <?php
              $sql_convocation_foot=mysql_query("SELECT * FROM `convocation_prev_convocations` WHERE is_del=0 order by rand() limit 2") or die(mysql_error());
              while($row_conv_foot=mysql_fetch_assoc($sql_convocation_foot))
              {
              ?>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(administrator/upload/prev_convocations/<?php echo $row_conv_foot['image'] ?>);"></a>
                <div class="text">
                  <h3 class="heading"><a href="javascript:void(0)"><?php echo $row_conv_foot['name'] ?></a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span>  <?php echo date('M d, Y',strtotime($row_conv_foot['start_date']));  ?></a></div>
                    <div style="display:none;"><a href="#"><span class="icon-person"></span> Admin</a></div>
                    <div style="display:none;"><a href="#"><span class="icon-chat"></span> 19</a></div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-2">
             <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Site Links</h2>
              <ul class="list-unstyled">
                <li><a href="index.php" class="py-2 d-block">Home</a></li>
                <li><a href="about.php" class="py-2 d-block">University</a></li>
                <li><a href="teacher.php" class="py-2 d-block">Guests</a></li>
<!--                <li><a href="#" class="py-2 d-block">Students</a></li>-->
<!--                <li><a href="#" class="py-2 d-block">Video</a></li>-->
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text"><?php echo $setting['address'] ?></span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text"><?php echo $setting['phone'] ?></span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text"><?php echo $setting['email']; ?></span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved |  by <a href="<?php echo $setting['website_link']; ?>" target="_blank"><?php echo $setting['website']; ?></a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
      
<style>
    .ftco-section-parallax{
        display:none;
    }
    .ftco-freeTrial{
        display:none
    }
    input.parsley-error, select.parsley-error, textarea.parsley-error {

    background: #FAEDEC;
    border: 1px solid #E85445;

}
ul.parsley-errors-list {

    list-style: none;
    color: #E74C3C;
    padding-left: 0;

}
ul.parsley-errors-list {

    list-style: none;
    color: #E74C3C;

}
</style>    
<script>
  function init_parsley() {
			
			if( typeof (parsley) === 'undefined'){ return; }
			console.log('init_parsley');
			
			$/*.listen*/('parsley:field:validate', function() {
			  validateFront();
			});
			$('#reg_form .btn').on('click', function() {
                        
			  $('#reg_form').parsley().validate();
			  validateFront();
			});
			var validateFront = function() {
			  if (true === $('#reg_form').parsley().isValid()) {
				$('.bs-callout-info').removeClass('hidden');
				$('.bs-callout-warning').addClass('hidden');
			  } else {
				$('.bs-callout-info').addClass('hidden');
				$('.bs-callout-warning').removeClass('hidden');
			  }
			};
		  
			$/*.listen*/('parsley:field:validate', function() {
			  validateFront();
			});
			
			
			
			  try {
				hljs.initHighlightingOnLoad();
			  } catch (err) {}
			
		};  
   $(document).ready(function(){
   init_parsley();    
   })             
  </script>
  <!-- loader -->
<!--  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>-->


  