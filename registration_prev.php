<?php
ob_start();
session_start();
//include_once("administrator/includes/session.php");
include_once("administrator/includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
$setting=mysql_fetch_assoc(mysql_query("select * from `convocation_settings` where id=1"));
$cms=mysql_fetch_assoc(mysql_query("select * from `convocation_cms` where id=4"));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Leading - University</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
  <?php include 'header.php'; ?>
 
    <section class="ftco-section contact-section ftco-degree-bg bg-light">
        
      <div class="container">
       
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">Registration Guidelines</h2>
          </div>
            <?php 
         if(!empty($_SESSION['success_alert']))
         {
           echo $_SESSION['success_alert'];
         
         }
         unset($_SESSION['success_alert']);
        ?> 
        <?php 
         if(!empty($_SESSION['flash_msg']))
         {
           echo $_SESSION['flash_msg'];
           unset($_SESSION['flash_msg']);
         
         }
         
        ?>   
          <div class="w-100"></div>
          <?php echo $cms['description']; ?>
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
          	
                <form action="registration.php" method="post">
                    <div class="form-group">
                        <input type="checkbox" value="1" required=""> Accepts
                    </div>
                    <div class="form-group">
                    <input type="submit" value="Next" name="next" class="btn btn-primary py-3 px-5">
                    </div>
            </form>
          
          </div>

            
        </div>
      </div>
    </section>
		
		

   
    
  

  <!-- loader -->
<?php include 'footer.php'; ?>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  
  
  </body>
</html>