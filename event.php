<?php
ob_start();
//include_once("administrator/includes/session.php");
include_once("administrator/includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Leading - University</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
   <?php include 'header.php'; ?>
   <div class="hero-wrap hero-wrap-2" style="background-image: url('images/_banner.jpg'); background-attachment:fixed;display:none;">
    <div class="overlay" style=" opacity:0 !important;"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-8 ftco-animate text-center">
<!--            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home</a></span> <span>C</span></p>-->
<!--            <h1 class="mb-3 bread">About</h1>-->
          </div>
        </div>
      </div>
    </div> 
     
    <section class="ftco-section bg-light" style="padding:3em 0;">
        
    	<div class="container">
            <h2 class="mb-4" style="text-align:center;">Previous Convocation</h2> 
            <?php
            $sql_prev_conv=mysql_query("select * from `convocation_prev_convocations` where is_del=0 order by sort_order") or die(mysql_error());
            while($row_prev_conv=mysql_fetch_assoc($sql_prev_conv))
            {
                $prev_convocations[]=$row_prev_conv;
            }
            $count=0;
            $total_row=mysql_num_rows($sql_prev_conv);
            $total_i=ceil($total_row/3);
            
            ?>
            <?php for($i=1;$i<=$total_i;$i++){ ?>
            <div class="row">
             <?php 
             for($j=1;$j<=3;$j++){ 
            $convocation=$prev_convocations[$count];    
            if(!empty($convocation))
            {
            ?>  
             <?php if($j==2){ ?>    
             <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry d-md-flex align-self-stretch flex-column-reverse">
              <a href="javascript:void(0)" class="block-20 align-self-end" style="background-image: url('administrator/upload/prev_convocations/<?php echo $convocation['image'] ?>');">
              </a>
              <div class="text p-4 d-block">
              	<div class="meta mb-3">
                  <div><a href="#"><?php echo date('M. d, Y',strtotime($convocation['start_date'])); ?></a></div>
                  <div style="display:none;"><a href="#">Admin</a></div>
                  <div style="display:none;"><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading mb-4"><a href="#"><?php echo $convocation['name']; ?></a></h3>
                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> <?php echo date('h:iA',strtotime($convocation['start_time'])) ?>-<?php echo date('h:iA',strtotime($convocation['end_time'])) ?></span> <span><i class="icon-map-o"></i> <?php echo $convocation['location']; ?></span></p>
                <p><?php echo substr(strip_tags($convocation['about']), 0, 170); ?></p>
                <p style="display:none;"><a href="javascript:void(0)">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
              </div>
            </div>
          </div>
             <?php }else{ ?>     
        	<div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry align-self-stretch">
              <a href="javascript:void(0)" class="block-20" style="background-image: url('administrator/upload/prev_convocations/<?php echo $convocation['image'] ?>');">
              </a>
              <div class="text p-4 d-block">
              	<div class="meta mb-3">
                  <div><a href="#"><?php echo date('M. d, Y',strtotime($convocation['start_date'])); ?></a></div>
                  <div style="display:none;"><a href="#">Admin</a></div>
                  <div style="display:none;"><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
                <h3 class="heading mb-4"><a href="#"><?php echo $convocation['name']; ?></a></h3>
                <p class="time-loc"><span class="mr-2"><i class="icon-clock-o"></i> <?php echo date('h:iA',strtotime($convocation['start_time'])) ?>-<?php echo date('h:iA',strtotime($convocation['end_time'])) ?></span> <span><i class="icon-map-o"></i> <?php echo $convocation["location"]; ?></span></p>
                <p><?php echo substr(strip_tags($convocation['about']), 0, 170); ?></p>
                <p style="display:none;"><a href="javascript:void(0)">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
              </div>
            </div>
          </div>
           <?php }?>    
             <?php $count++;}}?>   
        </div>
             <?php } ?>    
            <div class="row mt-5" style="display:none;">
          <div class="col text-center">
            <div class="block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div>
    	</div>
    </section>
		
      <section class="ftco-section-parallax" >
      <div class="parallax-img d-flex align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
              <h2>Subcribe to our Newsletter</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
              <div class="row d-flex justify-content-center mt-5">
                <div class="col-md-8">
                  <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                      <input type="text" class="form-control" placeholder="Enter email address">
                      <input type="submit" value="Subscribe" class="submit px-3">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include 'footer.php'; ?>  
    <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>  
  </body>
</html>