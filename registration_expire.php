<?php
ob_start();
session_start();
//include_once("administrator/includes/session.php");
include_once("administrator/includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
$setting=mysql_fetch_assoc(mysql_query("select * from `convocation_settings` where id=1"));
$cms=mysql_fetch_assoc(mysql_query("select * from `convocation_cms` where id=4"));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Leading - University</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
  <?php include 'header.php'; ?>
  <div class="hero-wrap hero-wrap-2" style="background-image: url('images/about_banner.jpg'); background-attachment:fixed;display:none;">
    <div class="overlay" style=" opacity:0 !important;"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-8 ftco-animate text-center">
<!--            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home</a></span> <span>C</span></p>-->
<!--            <h1 class="mb-3 bread">About</h1>-->
          </div>
        </div>
      </div>
    </div>
    <section class="ftco-section contact-section ftco-degree-bg bg-light">
      <div class="container">
       <?php 
         if(!empty($_SESSION['success_alert']))
         {
           echo $_SESSION['success_alert'];
         
         }
         unset($_SESSION['success_alert']);
        ?> 
        <?php 
         if(!empty($_SESSION['flash_msg']))
         {
           echo $_SESSION['flash_msg'];
           unset($_SESSION['flash_msg']);
         
         }
         
        ?>   
       <div class="alert alert-danger" id="error_msg" style="display:none;">
        <span id="msg_txt"></span> 
       </div>   
        <div class="row d-flex  contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4"></h2>
          </div>
          <div class="w-100"></div>
          
        </div>
         <div class="row block-9">
          <div class="col-md-6 pr-md-5">
              <form action="" method="post" id="reg_form" >
                  <h2>Registration closed for 3rd Convocation  </h2>
                  
              <?php //echo $cms['description']; ?>    
         
              
           
           
              
              
            </form>
          
          </div>

             <div class="col-md-6" ><img src="images/about_banner.jpg" style="height:600px;width:600px;"></div>
        </div>  
      </div>
    </section>
		
		

   
    
  

  <!-- loader -->
<?php include 'footer.php'; ?>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    

 
  
  
  </body>
</html>