<?php
ob_start();
session_start();
//include_once("administrator/includes/session.php");
include_once("administrator/includes/config.php");
$url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
$setting=mysql_fetch_assoc(mysql_query("select * from `convocation_settings` where id=1"));
if(isset($_REQUEST['submit']))
{
   $name = isset($_POST['name']) ? $_POST['name'] : '';
   $email = isset($_POST['email']) ? $_POST['email'] : '';
   $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
   $message = isset($_POST['message']) ? $_POST['message'] : '';
   $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
   $admin=mysql_fetch_assoc(mysql_query("select * from `convocation_admin` where id=1"));
   //echo "<pre>";print_r($admin);exit;
   $tpl_message="Hello Admin,<br/><br/>";
   $tpl_message.="New Query<br/><br/>";
   $tpl_message.="Name:".$name."<br/><br/>";
   $tpl_message.="Email:".$email."<br/><br/>";
   $tpl_message.="Phone:".$phone."<br/><br/>";
   $tpl_message.="Sebject:".$subject."<br/><br/>";
   $tpl_message.="Message:".$message."<br/><br/>";
   $tpl_message.="Thanks & Regards<br/><br/>";
   $tpl_message.="Convocation Team Leading University";
   $mail_subject="Query Request:".$subject;
   if(send_email($admin['email'],"Query Request",$tpl_message)) 
   {
       
      
      
   }
   $_SESSION['success_alert']="<div class='alert alert-success'>
            Your message has been sent successfully.
          </div>";
 //  header("Location:contact.php");
   
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Leading - University</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
  <?php include 'header.php'; ?>
  <div class="hero-wrap hero-wrap-2" style="background-image: url('images/about_banner.jpg'); background-attachment:fixed;display:none;">
    <div class="overlay" style=" opacity:0 !important;"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
          <div class="col-md-8 ftco-animate text-center">
<!--            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home</a></span> <span>C</span></p>-->
<!--            <h1 class="mb-3 bread">About</h1>-->
          </div>
        </div>
      </div>
    </div>
    <section class="ftco-section contact-section ftco-degree-bg bg-light">
        
      <div class="container">
       
        <div class="row d-flex mb-5 contact-info">
            
          <div class="col-md-12 mb-4">
            <h2 class="h4">Contact Information</h2>
          </div>
            
          <div class="w-100"></div>
          <div class="col-md-3">
            <p><span>Address:</span> <?php echo $setting['address'] ?></p>
          </div>
          <div class="col-md-3">
            <p><span>Phone:</span> <a href="tel://<?php echo $setting['phone'] ?>"><?php echo $setting['phone'] ?></a></p>
          </div>
          <div class="col-md-3">
            <p><span>Email:</span> <a href="mailto:<?php echo $setting['email'] ?>"><?php echo $setting['email'] ?></a></p>
          </div>
          <div class="col-md-3">
            <p><span>Website</span> <a href="#"><?php echo $setting['website'] ?></a></p>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
               <?php 
        echo $_SESSION['success_alert'];
        unset($_SESSION['success_alert']);
        
        ?>  
          	<h4 class="mb-4">Do you have any questions?</h4>
                <form action="" method="post">
              <div class="form-group">
                  <input type="text" class="form-control" placeholder="Your Name" required="" name="name">
              </div>
              <div class="form-group">
                  <input type="email" class="form-control" placeholder="Your Email" required="" name="email">
              </div>
               <div class="form-group">
                  <input type="text" class="form-control" placeholder="Your Phone" required="" name="phone">
              </div>     
              <div class="form-group">
                  <input type="text" class="form-control" placeholder="Subject" required="" name="subject">
              </div>
              <div class="form-group">
                <textarea name="message" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                  <input type="submit" value="Send Message" name="submit" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>

            <div class="col-md-6">
             <?php
             $content=mysql_fetch_assoc(mysql_query("select * from `convocation_cms` where id=3"));
             echo $content['map_for_web'];
             ?>
                
            </div>
        </div>
      </div>
    </section>
		
		<section class="ftco-section-parallax">
      <div class="parallax-img d-flex align-items-center">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
              <h2>Subcribe to our Newsletter</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
              <div class="row d-flex justify-content-center mt-5">
                <div class="col-md-8">
                  <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                      <input type="text" class="form-control" placeholder="Enter email address">
                      <input type="submit" value="Subscribe" name="submit" class="submit px-3">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

   
    
  

  <!-- loader -->
<?php include 'footer.php'; ?>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  
  
  </body>
</html>