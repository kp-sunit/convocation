<?php
$name='NikoshBAN';
$type='TTF';
$desc=array (
  'CapHeight' => 561,
  'XHeight' => 399,
  'FontBBox' => '[-478 -276 1102 1039]',
  'Flags' => 4,
  'Ascent' => 859,
  'Descent' => -276,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 479,
);
$unitsPerEm=1024;
$up=-109;
$ut=49;
$strp=250;
$strs=50;
$ttffile='/home/skillstech/public_html/projects/university_management/mpdf60/ttfonts/NikoshBAN.ttf';
$TTCfontID='0';
$originalsize=797504;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='nikosh';
$panose=' a f 2 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 783, 197, 98
// usWinAscent/usWinDescent = 859, -308
// hhea Ascent/Descent/LineGap = 859, -308, 0
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'beng' => 'DFLT ',
);
$GSUBFeatures=array (
  'beng' => 
  array (
    'DFLT' => 
    array (
      'blwf' => 
      array (
        0 => 0,
      ),
      'rphf' => 
      array (
        0 => 1,
      ),
      'half' => 
      array (
        0 => 2,
      ),
      'pstf' => 
      array (
        0 => 3,
      ),
      'vatu' => 
      array (
        0 => 4,
      ),
      'pres' => 
      array (
        0 => 5,
      ),
      'abvs' => 
      array (
        0 => 6,
      ),
      'blws' => 
      array (
        0 => 7,
      ),
      'psts' => 
      array (
        0 => 8,
      ),
      'haln' => 
      array (
        0 => 9,
      ),
      'init' => 
      array (
        0 => 10,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 789840,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 789882,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 789912,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 790362,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 790380,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 790794,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 792540,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 793176,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 4,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 796316,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 796550,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 797000,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'beng' => 'DFLT ',
);
$GPOSFeatures=array (
  'beng' => 
  array (
    'DFLT' => 
    array (
      'abvm' => 
      array (
        0 => 0,
        1 => 1,
        2 => 2,
      ),
      'blwm' => 
      array (
        0 => 3,
        1 => 4,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 786926,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 8,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 787298,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 787354,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 787796,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 788064,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 788270,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>