<?php
   ob_start();
   include_once("./includes/session.php");
   //include_once("includes/config.php");
   include_once("./includes/config.php");
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
           function correctImageOrientation($filename) {
            if (function_exists('exif_read_data')) {
        $exif = exif_read_data($filename);
        if($exif && isset($exif['Orientation'])) {
        $orientation = $exif['Orientation'];
        if($orientation != 1){
        $img = imagecreatefromjpeg($filename);
        $deg = 0;
        switch ($orientation) {
          case 3:
            $deg = 180;
            break;
          case 6:
            $deg = 270;
            break;
          case 8:
            $deg = 90;
            break;
        }
        if ($deg) {
          $img = imagerotate($img, $deg, 0);       
        }
        // then rewrite the rotated image back to the disk as $filename
        imagejpeg($img, $filename, 95);
        } // if there is some rotation necessary
        } // if have the exif orientation info
        } // if function exists     
        }
   
   if(isset($_GET['action']) && $_GET['action']=='delete')
   
   {
   
   $item_id=$_GET['cid'];
   
   $deleteQry = "DELETE FROM `convocation_student` WHERE `id` = '" . mysql_real_escape_string($item_id) . "'";
   
   mysql_query($deleteQry);	
   
   header('Location:list_all_students.php');
   
   exit();
   
   }
   
   if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete_all')
   
   {
   
   $item_id=implode(",",$_REQUEST['ids']);
   
   $deleteQry = "DELETE FROM `convocation_student` WHERE `id` IN (" . mysql_real_escape_string($item_id) . ")";
   
   //$deleteQry="UPDATE `classname` set `status`='1' WHERE `id` = '" . mysql_real_escape_string($item_id) . "'"; 
   
   mysql_query($deleteQry) or die(mysql_error());	
   
   header('Location:list_all_students.php');
   
   //exit();
   
   }
   
     ?>
<!--		<link  href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
   <link  href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>-->
<script language="javascript">
   function del(aa,bb)
   
   {
   
   var a=confirm("Are you sure, you want to delete this?")
   
   if (a)
   
   {
   
   location.href="list_all_students.php?cid="+ aa +"&action=delete"
   
   }  
   
   } 
   
   
   
   function inactive(aa)
   
   { 
   
   location.href="list_all_students.php?cid="+ aa +"&action=inactive"
   
   
   
   } 
   
   function active(aa)
   
   {
   
   location.href="list_all_students.php?cid="+aa+"&action=active";
   
   } 
   
   
   
</script>
<?php include("includes/header.php"); ?>
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php include("includes/left_panel.php"); ?>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
   <div class="page-content">
      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- /.modal -->
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN STYLE CUSTOMIZER -->
      <!-- END STYLE CUSTOMIZER -->
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title">
         All Students
      </h3>
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <i class="fa fa-home"></i>
               <a href="index.php">Home</a>
               <i class="fa fa-angle-right"></i>
            </li>
            <li>
               <a href="#">List All Students</a>
               <i class="fa fa-angle-right"></i>
            </li>
            <!--<li>
               <a href="#">Editable Datatables</a>
               
               </li>-->
         </ul>
         <div class="btn-group" style="float:right;display:none;">
            <a id="sample_editable_1_new" class="btn blue" href="add_students.php">
            Add New  <i class="fa fa-plus"></i>
            </a>
         </div>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
         <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="fa fa-gift"></i>List All Students
                     <!--<i class="fa fa-edit"></i>Editable Table-->
                  </div>
                  <!--<div class="tools">
                     <a href="javascript:;" class="collapse">
                     
                     </a>
                     
                     <!--<a href="#portlet-config" data-toggle="modal" class="config">
                     
                     </a>
                     
                     <a href="javascript:;" class="reload">
                     
                     </a>
                     
                     <a href="javascript:;" class="remove">
                     
                     </a>
                     
                     </div>-->
               </div>
               <div class="portlet-body">
                  <div class="table-toolbar">
                     <div class="row">
                        <div class="btn-group pull-right" style=" padding:5px;">
                           <!--									<div class="col-md-6">
                              <div class="btn-group pull-right">
                              
                              	<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                              
                              	</button>
                              
                              	<ul class="dropdown-menu pull-right">
                              
                              		<li>
                              
                              			<a href="#">
                              
                              			Print </a>
                              
                              		</li>
                              
                              		<li>
                              
                              			<a href="#">
                              
                              			Save as PDF </a>
                              
                              		</li>
                              
                              		<li>
                              
                              			<a href="#">
                              
                              			Export to Excel </a>
                              
                              		</li>
                              
                              	</ul>
                              
                              </div>
                              
                              </div>-->
                        </div>
                     </div>
                     <form method="post" id="delete_form">
                       <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                           <thead>
                              <tr role="row" class="heading">
                                <th>Student ID</th>
                                <th>Name</th>
                                <th>Semester</th>
                                <th>Degree</th>
                                <th>Department</th>
                                <th>C.G.P.A</th>
                                <th>Due</th>
                                <th>Action</th>        
                              </tr>
                              <tr role="row" class="filter">
                                  <td>
                                      <div class="margin-bottom-5">
                                       <input type="text" class="form-control form-filter input-sm" name="student_id" placeholder="Student ID"/>
                                    </div>
                                  </td>
                                 
                                       
                                 <td>
                                    <div class="margin-bottom-5">
                                       <input type="text" class="form-control form-filter input-sm" name="name" placeholder="Name"/>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="margin-bottom-5">
                                       <input type="text" class="form-control form-filter input-sm" name="semester" placeholder="Semester"/>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="margin-bottom-5">
                                        <select class="form-control form-filter input-sm" name="degree">
                                            <option value="">Degree</option>  
                                            <?php 
                                            $sql_degree=mysql_query("select * from `convocation_degree` where is_del=0") or die(mysql_error());
                                            while ($row_degree=mysql_fetch_assoc($sql_degree))
                                            {
                                            ?>
                                            <option value="<?php echo $row_degree['id'] ?>"><?php echo $row_degree['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="margin-bottom-5">
                                        <select class="form-control form-filter input-sm" name="depertment">
                                            <option value="">Department</option> 
                                            <?php 
                                            $sql_degree=mysql_query("select * from `convocation_depertment` where is_del=0") or die(mysql_error());
                                            while ($row_degree=mysql_fetch_assoc($sql_degree))
                                            {
                                            ?>
                                            <option value="<?php echo $row_degree['id'] ?>"><?php echo $row_degree['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                 </td>
                                 <td></td>
                                 <td></td>
                                 <td>
                                    <div class="margin-bottom-5">
                                       <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
                                 </td>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table> 
                       
                     </form>
                     <!--<input type="submit" class="btn btn-danger" name="bulk_delete_submit" value="Delete"/>-->
                  </div>
               </div>
               <!-- END EXAMPLE TABLE PORTLET-->
            </div>
         </div>
         <!-- END PAGE CONTENT -->
      </div>
   </div>
   <!-- END CONTENT -->
</div>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/global/scripts/datatable.js"></script>
<script src="assets/admin/pages/scripts/table-ajax-student.js"></script>
<script>
   jQuery(document).ready(function() {       
   
      Metronic.init(); // init metronic core components
   
   Layout.init(); // init current layout
   
   QuickSidebar.init(); // init quick sidebar
   
   Demo.init(); // init demo features
   TableAjax.init();
   
     // TableEditable.init();
   
   });
   
</script>
<script type="text/javascript">
   function deleteConfirm(){
       var result = confirm("Are you sure to delete this student?");
       if(result){
           return true;
       }else{
           return false;
       }
   }
   
</script>
<script>
   $(document).ready(function(){
       $(".san_open").parent().parent().addClass("active open");
   //     $(".dt-buttons").append("<a class='dt-button'   onclick=download_pdf('print_scheme.php')><span>PDF</span></a>");
      $(".dt-buttons").append('<a class="dt-button"  onclick=deleteConfirmAll()><span>Delete</span></a>');
       
       
         
   
   });
   function deleteConfirmAll()
   {
       check_length=parseInt($('.checkbox1:checked').length)
       if(check_length<=0)
       {
           alert("please checked atleast one");
       }
       else
       {
           var result = confirm("Are you want to delete all?");
            if(result){
               $("#delete_form").submit();
            }
       }
       
   }
   document.getElementById("focusElement").focus();
</script>
<style type="text/css">
   tfoot {
   display: table-header-group;
   }
   tfoot input {
   width: 100%;
   padding: 6px;
   box-sizing: border-box;
   font-size: 12px;
   }
</style>
<div class="page-footer">
   <?php include("includes/footer.php"); ?>
</div>
</body>
<!-- END BODY -->
</html>



