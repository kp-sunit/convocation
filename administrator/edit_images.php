<?php 
   ob_start(); 
   include_once("./includes/session.php");
   //include_once("includes/config.php");
   include_once("./includes/config.php");
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   ini_set('default_charset','utf-8');
   mysql_set_charset('utf8');
   header('Content-type: text/html; charset=utf-8');
   $categoryRowset=mysql_fetch_assoc(mysql_query("SELECT * FROM `convocation_registration` WHERE id='".$_REQUEST['id']."'"));

 if($_SESSION['admin_id']!=2){
   header('location:list_registrations.php');            
   exit;
 }
   
   if(isset($_REQUEST['submit_edit']))
   {
   $ref_no= isset($_REQUEST['ref_no'])?$_REQUEST['ref_no']:'';
   $fields=array(
       'student_first_name'=>mysql_real_escape_string($_REQUEST['student_first_name']),  
       );
 
 
       $fieldsList = array();
        foreach ($fields as $field => $value) {
                $fieldsList[] = '`' . $field . '`' . '=' . "'" . $value . "'";

        }

         $editQuery = "UPDATE `convocation_registration` SET " . implode(', ', $fieldsList)
                       . " WHERE `id` = '" . mysql_real_escape_string($_REQUEST['id']) . "'";
         mysql_query($editQuery);
   
   
       
      if($_FILES['profile_photo']['tmp_name']!='')
   {
   $target_path="upload/documents/";
   $userfile_name = $_FILES['profile_photo']['name'];  
   $userfile_tmp = $_FILES['profile_photo']['tmp_name']; 
   $img_name =time().$userfile_name;
   $img=$target_path.$img_name;
   move_uploaded_file($userfile_tmp, $img);
   mysql_query("UPDATE `convocation_registration` set photo='".$img_name."' WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'");
   }    
   
   
    if($_FILES['receipt']['tmp_name']!='')
   {
   $target_path="upload/documents/";
   $userfile_name = $_FILES['receipt']['name'];  
   $userfile_tmp = $_FILES['receipt']['tmp_name']; 
   $img_name =time().$userfile_name;
   $img=$target_path.$img_name;
   move_uploaded_file($userfile_tmp, $img);
   mysql_query("UPDATE `convocation_registration` set receipt='".$img_name."',ref_no='' WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'");
   }
   else{
   mysql_query("UPDATE `convocation_registration` set receipt='',ref_no='".$ref_no."' WHERE `id`='".mysql_real_escape_string($_REQUEST['id'])."'");
   }
   header('location:add_registration.php?id='.$_REQUEST['id'].'&action=view');    
   }
   ?>
<?php include('includes/header.php');?>
<!-- END HEADER -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include('includes/left_panel.php');?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <?php //include('includes/style_customize.php');?>
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title">Registration</h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a href="dashboard.php">Home</a>
                  <i class="fa fa-angle-right"></i>
               </li>
               <li>
                  <a href="#">Registration</a>
               </li>
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i>Edit Registration for <?php echo $categoryRowset['student_id']; ?>
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body">
                     <!-- BEGIN FORM-->
                      <form  class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                        <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                        <?php if($_REQUEST['action']==''){
                           $current_showed_rows=1;
                           }
                           else{
                           $current_showed_rows=10;
                           }?>
                        <input type="hidden" name="current_showed_rows" id="current_showed_rows" value="<?php echo $current_showed_rows;?>" />
                     
                        <div class="form-group">

                              <label class="col-md-3 control-label">Student's Name</label>

                              <div class="col-md-8">

                                  <input type="text" class="form-control" value="<?php echo $categoryRowset['student_first_name']; ?>" name="student_first_name">

                              </div>

                           </div>
                           <div class="form-group" >
                              <div style="" class="col-md-10">
                                 <div style="width: 353px;height: 236px;border:1px solid #ccc;margin:0 auto;">
                                    <img id="blah" src="<?php echo !empty($categoryRowset['photo'])?'upload/documents/'.$categoryRowset['photo']:'upload/no.png'; ?>"  style="width: 353px; height: 237px;" alt="Select Image" />
                                 </div>
                              </div>
                           </div>
                          
                            <div class="form-group">
                              <label class="col-md-3 control-label">Profile Photo</label>
                              <div class="col-md-5">
                                  <input type="file" class="form-control" <?php echo empty($categoryRowset)?'required':'' ?>  id="image"  name="profile_photo" accept="image/*">
                              </div>
                           </div> 
                        <?php 
                        //echo $categoryRowset['payment_method'];
                        //echo $categoryRowset['receipt'];
                        if($categoryRowset['payment_method']!='Rocket Pay'){ ?>
                        
                             <div class="form-group" >
                              <div style="" class="col-md-10">
                                 <div style="width: 353px;height: 236px;border:1px solid #ccc;margin:0 auto;">
                                    <img id="blah2" src="<?php echo !empty($categoryRowset['receipt'])?'upload/documents/'.$categoryRowset['receipt']:'upload/no.png'; ?>"  style="width: 353px; height: 237px;" alt="Select Image" />
                                 </div>
                              </div>
                           </div>
                          
                            <div class="form-group">
                              <label class="col-md-3 control-label">Bank Receipt</label>
                              <div class="col-md-5">
                                  <input type="file" class="form-control" <?php echo empty($categoryRowset)?'required':'' ?>  id="image2"  name="receipt" accept="image/*">
                              </div>
                           </div> 
                       <?php }else{ ?>
                       <div class="form-group">
                              <label class="col-md-3 control-label">Reference No</label>
                              <div class="col-md-5">
                                  <input type="text" class="form-control" name="ref_no" required="" value="<?php echo $categoryRowset['ref_no']; ?>">
                              </div>
                       </div> 
                       <?php }?>
                           <div class="form-actions fluid">
                              <div class="row">
                                 <div class="col-md-offset-3 col-md-8">
                                    <button class="btn blue" type="submit" name="submit_edit">UPDATE</button>     

                                 </div>
                              </div>
                           </div>
                        
                     </form> 
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
      </div>
   </div>
   <style>
      .thumb{
      height: 60px;
      width: 60px;
      padding-left: 5px;
      padding-bottom: 5px;
      }
   </style>
   <script>
      window.preview_this_image = function (input) {
      
      if (input.files && input.files[0]) {
      $(input.files).each(function () {
      var reader = new FileReader();
      reader.readAsDataURL(this);
      reader.onload = function (e) {
      $("#previewImg").append("<span><img class='thumb' src='" + e.target.result + "'><img border='0' src='../images/erase.png'  border='0' class='del_this' style='z-index:999;margin-top:-34px;'></span>");
      }
      });
      }
      }
   </script>
   <!-- END CONTENT -->
   <!-- BEGIN QUICK SIDEBAR -->
   <?php //include('includes/quick_sidebar.php');?>
   <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<link href="assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="assets/admin/pages/css/portfolio.css" rel="stylesheet" type="text/css"/>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/portfolio.js"></script>
<script>
   jQuery(document).ready(function() {    
      // initiate layout and plugins
      Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
      //FormSamples.init();
    Portfolio.init(); 
      
   });
   
   
       
</script>

<script>
   function readURL(input) {
   
    if (input.files && input.files[0]) {
      var reader = new FileReader();
   
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
   
      reader.readAsDataURL(input.files[0]);
    }
   }
   
   
      function readURL2(input) {
   
    if (input.files && input.files[0]) {
      var reader = new FileReader();
   
      reader.onload = function(e) {
        $('#blah2').attr('src', e.target.result);
      }
   
      reader.readAsDataURL(input.files[0]);
    }
   }
   
   $("#image").change(function() {
    readURL(this);
   });
   
   
      $("#image2").change(function() {
    readURL2(this);
   });
   
</script>

<?php include('includes/footer.php'); ?>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

