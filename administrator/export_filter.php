<?php 
   ob_start(); 
   include_once("./includes/session.php");
   //include_once("includes/config.php");
   include_once("./includes/config.php");
   $url=basename(__FILE__)."?".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'cc=cc');
   
   
   ?>
<?php include('includes/header.php');?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckfinder/ckfinder_v1.js"></script>
<!-- END HEADER -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
   <!-- BEGIN SIDEBAR -->
   <?php include('includes/left_panel.php');?>
   <!-- END SIDEBAR -->
   <!-- BEGIN CONTENT -->
   <div class="page-content-wrapper">
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- /.modal -->
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN STYLE CUSTOMIZER -->
         <?php //include('includes/style_customize.php');?>
         <!-- END STYLE CUSTOMIZER -->
         <!-- BEGIN PAGE HEADER-->
         <h3 class="page-title">Managing Registrations</h3>
         <div class="page-bar">
            <ul class="page-breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a href="dashboard.php">Home</a>
                  <i class="fa fa-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:void(0)">Managing Registrations</a>
                  <i class="fa fa-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:void(0)">Export Registrations</a>
               </li>
            </ul>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->
         <div class="row">
            <div class="col-md-12">
               <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="fa fa-gift"></i> Export Registrations
                     </div>
                     <div class="tools">
                     </div>
                  </div>
                  <div class="portlet-body form">
                     <!-- BEGIN FORM-->
                     <form  class="form-horizontal" id="form_sample_1" method="post" action="export_reg.php" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                        <input type="hidden" name="action" value="<?php echo $_REQUEST['action'];?>" />
                        <?php if($_REQUEST['action']==''){
                           $current_showed_rows=1;
                           }
                           else{
                           $current_showed_rows=10;
                           }?>
                        <input type="hidden" name="current_showed_rows" id="current_showed_rows" value="<?php echo $current_showed_rows;?>" />
                        <div class="form-body" style="position:relative;">
                           
                       
                            <div class="form-group">
                              <label class="col-md-3 control-label">Student ID</label>
                              <div class="col-md-5">
                                  <input type="text" class="form-control" name="student_id">
                              </div>
                           </div> 
                             <div class="form-group">
                              <label class="col-md-3 control-label">Registration For</label>
                              <div class="col-md-5">
                              <select class="form-control" name="registration_for">
                                            <option value="">Registration For</option>
                                            <option value="one_certificate">One Certificate</option>
                                            <option value="two_certificate">Two Certificate</option>
                              </select>    
                              </div>
                           </div> 
                            <div class="form-group">
                              <label class="col-md-3 control-label">Degree</label>
                              <div class="col-md-5">
                              <select class="form-control form-filter input-sm" name="degree_id">
                                            <option value="">Degree</option>  
                                            <?php  
                                            $sql_degree=mysql_query("select * from `convocation_degree` where is_del=0");
                                            while($row_degree=mysql_fetch_assoc($sql_degree)){
                                            ?>
                                            <option value="<?php echo $row_degree['id'] ?>"><?php echo $row_degree['name'] ?></option>
                                            <?php } ?>
                                                                                        
                              </select>  
                              </div>
                           </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Department</label>
                              <div class="col-md-5">
                              <select class="form-control form-filter input-sm" name="depertment_id">
                                            <option value="">Department</option>  
                                            <?php  
                                            $sql_degree=mysql_query("select * from `convocation_depertment` where is_del=0");
                                            while($row_dept=mysql_fetch_assoc($sql_degree)){
                                            ?>
                                            <option value="<?php echo $row_dept['id'] ?>"><?php echo $row_dept['name'] ?></option>
                                            <?php } ?>
                                                                                        
                              </select>  
                              </div>
                           </div>
                            <div class="form-group">
                              <label class="col-md-3 control-label">Phone</label>
                              <div class="col-md-5">
                                  <input type="text" class="form-control" name="phone">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-3 control-label">Status</label>
                              <div class="col-md-5">
                                  <select class="form-control" name="is_approved">
                                         <option value="">Status</option>
                                         <option value="1">Approved</option>
                                         <option value="0">Pending</option>
                                 </select>
                              </div>
                           </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Payment Status</label>
                              <div class="col-md-5">
                                  <select class="form-control form-filter input-sm" name="paid_status">
                                         <option value="">Payment Status</option>
                                         <option value="1">Paid</option>
                                         <option value="0">Pending</option>
                                         </select>
                              </div>
                           </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">Start Date</label>
                              <div class="col-md-5">
                                  <input type="text" class="form-control datepicker" name="start"  placeholder="dd-mm-yyyy" >
                              </div>
                              
                           </div>    
                        
                        <div class="form-group">
                         <label class="col-md-3 control-label">End Date</label>
                              <div class="col-md-5">
                                  <input type="text" class="form-control datepicker" name="end"  placeholder="dd-mm-yyyy" >
                              </div>   
                        </div>
                        <div class="form-actions fluid">
                           <div class="row">
                              <div class="col-md-offset-3 col-md-9">
                                  <button type="submit" class="btn blue"  name="submit">Submit</button>
                              </div>
                           </div>
                        </div>
                     </form>
                     <!-- END FORM-->
                  </div>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->
      </div>
   </div>
   <style>
      .thumb{
      height: 60px;
      width: 60px;
      padding-left: 5px;
      padding-bottom: 5px;
      }
   </style>
   <!-- END CONTENT -->
   <!-- BEGIN QUICK SIDEBAR -->
   <?php //include('includes/quick_sidebar.php');?>
   <!-- END QUICK SIDEBAR -->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include('includes/footer.php'); ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<link href="summernote/summernote.css" rel="stylesheet">

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="summernote/summernote.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
   jQuery(document).ready(function() {    
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   QuickSidebar.init(); // init quick sidebar
   Demo.init(); // init demo features
   
   
   
   });
     
   $(document).ready(function(){
   $('.summernote').summernote({
   height: "150px",
    
                    
  });        
   });  
         
</script>

<script>
   function readURL(input) {
   
    if (input.files && input.files[0]) {
      var reader = new FileReader();
   
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
   
      reader.readAsDataURL(input.files[0]);
    }
   }
   
   $("#image").change(function() {
    readURL(this);
   });
</script>
<script type="text/javascript">
   function populateState()
   {
   country_id=$("#country_id").val();
   $params={country_id:country_id};
   $.post("ajaxGetState.php",$params,function(result){
   $("#state_id").html(result.states);  
   $("#city_id").html('<option value="">Select City</option>');   
   },"json");
   } 
   
   
   
   function populateSubcategory()
   {
   category_id=$("#category_id").val();
   $params={category_id:category_id};
   $.post("ajaxGetSubcategory.php",$params,function(result){
   $("#subcategory_id").html(result.subcategory);   
   },"json");
   } 
   
   
   function populateCity()
   {
   state_id=$("#state_id").val();
   country_id=$("#country_id").val();
   $params={country_id:country_id,state_id:state_id};
   $.post("ajaxGetCity.php",$params,function(result){
   $("#city_id").html(result.city);    
   },"json");
   } 
   
   function populatePapers()
   {
   state_id=$("#state_id").val();
   country_id=$("#country_id").val();
   city_id=$("#city_id").val();
   $params={country_id:country_id,state_id:state_id,city_id:city_id};
   $.post("ajaxGetPapers.php",$params,function(result){
   $("#paper_id").html(result.slider);    
   },"json");
   } 
   
   
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({dateFormat:'dd-mm-yy'});
  } );
  </script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

