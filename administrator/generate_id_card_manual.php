<?php
include_once("includes/config.php");
$content="";
   
$content='<html>;
<head>
<title>ID Card</title>
<meta name="" content="">
</head>
<body>';
   
    
    $i=1;
     $content.='<div style="width:13%; margin:0 auto;">
    <button type="button" onclick="printDiv()" style="width:150px; margin-top:25px;text-align:center;">Print</button> 
    </div>';
    $content.='<div id="printable_div">';
    
     $imglink="upload/no-user-female.png";
 
        
        $content.='<div class="page-wrap" style="width:45%;">
            <table class="id-table"  cellpadding="15" cellspacing="0" width="100%"  style="background: url('.SITE_URL.'convocation-id-card-2/images/bg_2.png) no-repeat bottom center !important;">
			<tbody>
                                
    
				<tr class="id-top">
					<td class="logo-area">
                                            <div class="top-image"><img src="convocation-id-card-2/images/university_logo.png" alt="" style="width:100px;"></div>
					</td>
					<td align="center" class="title-area">
						<div class="top-content">
                                                    <img src="convocation-id-card-2/images/main.png" alt="" class="main-image" style="">
						</div>
					</td>
					<td class="logo-area" align="right">
                                            <div class="top-image"><img src="convocation-id-card-2/images/logo2.png" alt="" style="width:100px;"></div>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<div class="student-image"><img src="'.$imglink.'" alt="" ></div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table class="id-table center-table" cellpadding="15" cellspacing="0" >
							<tr class="table-row" stle="line-height:10px">
								<td class="title">Name:</td>
								<td class="desc" colspan="2" style="width:150px;"></td>
							</tr>
							<tr class="table-row" stle="line-height:10px">
								<td class="title">ID:</td>
								<td class="desc" colspan="2" style="width:150px;"></td>
							</tr>
							<tr class="table-row" stle="line-height:10px">
								<td class="title">Degree:</td>
								<td class="desc" colspan="2" style="width:150px;"></td>
							</tr>
						</table>
					</td>
					
				</tr>
                          
			</tbody>
		</table>
	</div>';
       if($i%2==0)
        {
            $content.='<div style="page-break-after: always"></div>'; 
        }
          
        
       $i++; 
    
    
   
$content.="
<style>    
@import url('https://fonts.googleapis.com/css?family=Roboto:400,500,600');
.page-wrap{
	max-width: 400px;
	padding: 0 15px;
	margin: 0 auto;
	font-family: 'Roboto', sans-serif;
}
.id-table{
	border:1px solid #acacac;
	padding: 0;
	margin: 15px 0;
	border-collapse: collapse;
	background: url('../images/bg.png') no-repeat bottom center;
	background-size: contain;
}
.id-top .top-image img{
	width: 150px;
}
.top-content{
	text-align: center;
}
.top-content h2{
	font-size:30px;
	margin: 0;
	line-height: 1.5;
	font-weight: 600;
}
.top-content p{
	font-size:24px;
	margin: 0;
	line-height: 1.4;
	font-weight: 500;
}
.id-table img{
	max-width: 100%;
}
.id-table .main-image{
	max-width: 60%;
}
.id-table .student-image{
	border:1px solid #b2b2b2;
	padding: 5px;
	width:130px;
	height:150px;
	background: rgba(255,255,255,1);
}
.id-table .student-image img{
	width:100%;
	height:100%;
	object-fit:cover;
}
.id-table .logo-area{
	width:25%
}
.id-table .title-area{
	width:50%
}
.id-table .title{
	width:40%
}
.id-table .desc{
	width:60%
}
.id-table .table-row{
	border: 1px solid #b2b2b2;
}
.id-table .table-row .title{
	font-weight: 500;
}
.id-table .table-row .desc{
	font-weight: 400;
}
.center-table{
	margin: 0 auto;
	margin-bottom: 40px;
	background-image: none;
	background-color: rgba(255,255,255,0.8)
}
@page { margin-top: 4px; 
       
        margin-header: 0mm; /* <any of the usual CSS values for margins> */
        margin-footer: 0mm; /* <any of the usual CSS values for margins> */
        }

</style>";  
     
$content.='</body>';
$content.='</html>';
?>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
function printDiv() {
   var restorepage = $('body').html();
var printcontent = $('#printable_div').clone();
$('body').empty().html(printcontent);
window.print();
$('body').html(restorepage);
}

</script>    
<?php die($content); ?>
