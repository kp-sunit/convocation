<?php
ob_start();

include_once('includes/session.php');

include_once("includes/config.php");

include_once("includes/functions.php");

include("chat/chat.php");



$login_time= $_SESSION['login_time'];

$current_time= strtotime(gmdate("Y-m-d H:i:s"));

$diff=$current_time-$login_time;

$currentFile = $_SERVER["PHP_SELF"];

$parts = explode('/', $currentFile);

$url=$_SERVER["REQUEST_URI"];

$_SESSION['prev_url']=$url;

//die($_SESSION['prev_url']);

//echo ($diff);exit;

if($diff>6000)

{

    

    header("Location:login_lock.php");

}

else{

   $login_time= $login_time+6000;

   $_SESSION['login_time']=$login_time; 



}

?>

<!DOCTYPE html>

<!-- 

Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2

Version: 3.6.2

Author: KeenThemes

Website: http://www.keenthemes.com/

Contact: support@keenthemes.com

Follow: www.twitter.com/keenthemes

Like: www.facebook.com/keenthemes

Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes

License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

  <!--<![endif]-->

  <!-- BEGIN HEAD -->

  <head>

    <?php

$pagename12 = end(explode('/', $_SERVER['REQUEST_URI']));

$pagename22 = explode('.', $pagename12);

$curpagename = $pagename22[0];

$curpagename = str_replace("_", " ", $curpagename);

?>

    <meta charset="utf-8"/>

    <title>

      <?php echo ucfirst($curpagename); ?>

    </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <meta content="" name="description"/>

    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>

    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->

    <link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->

    <link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>

    <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>

    <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>

    <link id="style_color" href="assets/admin/layout/css/themes/blue.css" rel="stylesheet" type="text/css"/>

    <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

    <!-- END THEME STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>

    <style>

      h3.page-title{

        display:none;

      }

	  

	 .caption{

	  font-weight:bold !important;

	  font-size:20px !important;

	  color:#217ebd !important; 

	  }



.portlet > .portlet-title {

    border-bottom: 1px solid #217ebd;

}





	  

	  tfoot input{

	  border:1px solid #ddd !important;

	  font-weight: normal !important;

	  }

	.table tr.heading > th {

	background-color: #217ebd !important;

	/*background-color: #eee !important;*/

	font-size: 18px !important;

	font-weight: 800 !important;

	text-align:center !important;

	color:#fff;

	

	}

	

	.table{

	text-align:center;

	}

	

	.table-striped>tbody>tr:nth-of-type(odd) {

	background-color: #fff !important;

	}

/*	.table thead tr th {

    font-size: 14px !important;

    font-weight: 600 !important;

}*/



.font-blue {

    color: #d64635 !important;

}



.page-header.navbar {

    height: 70px;

    min-height: 70px;

}



.page-header-fixed .page-container {

    margin-top: 70px;

}



table.table-bordered.dataTable {

    border-collapse: collapse !important;

}



table.table-bordered tbody th, table.table-bordered tbody td {

    color: #000 !important;

}



.dt-button{

    margin-left: 5px;

    background-color: #217ebd;

	color:#fff;

	display: inline-block;

    padding: 6px 12px;

    margin-bottom: 0;

    font-size: 14px;

    font-weight: 400;

    line-height: 1.42857143;

    text-align: center;

    white-space: nowrap;

    vertical-align: middle;

    -ms-touch-action: manipulation;

    touch-action: manipulation;

    cursor: pointer;

}



.dt-button:hover{

	color:#fff;

	text-decoration:none;



}



.blue.btn {

    color: #FFFFFF;

    background-color: #217ebd;

}

    </style>

  </head>

  <!-- END HEAD -->

  <!-- BEGIN BODY -->

  <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->

  <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->

  <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->

  <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->

  <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->

  <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->

  <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->

  <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->

  <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

  <body class="page-header-fixed page-quick-sidebar-over-content ">

    <!-- BEGIN HEADER -->

    <div class="page-header navbar navbar-fixed-top">

      <!-- BEGIN HEADER INNER -->

      <div class="page-header-inner">

        <!-- BEGIN LOGO -->
	        <div class="col-md-1 col-sm-1 col-xs-2">

	          <?php 

	$sqlpro=mysql_query("SELECT * FROM `grivance_adminprofile` where userid=1"); 
	$rowpro=mysql_fetch_array($sqlpro);

	?>

	          <?php 

	$sqladmin=mysql_query("SELECT * FROM `grivance_admin` where id=1"); 
	$rowadmin=mysql_fetch_array($sqladmin);

	?>

	          <a href="dashboard.php" class="main-logo">

	            <?php

	//  $fetch_logo=mysql_fetch_array(mysql_query("select * from `dimri_sitesettings` where `id`=1"));

	?>

	            <?php if($rowadmin['image']==''){

	?>

	           <?php /*?> <img src=""  alt="logo" class="logo-default" style="height:25px; width:100px;" /><?php */?>

	            <?php }else{ 

	$image_link=SITE_URL.'upload/documents/'.$rowadmin['image'];

	?>

	        <?php /*?>    <img src="<?php echo $image_link ?>" alt="logo" class="logo-default" style="height:25px;" /><?php */?>

	            <?php }?> 

				

                      <img src="<?php //echo $image_link;?>logo.png" alt="logo"/>	

	          </a>

	          <div class="menu-toggler sidebar-toggler hide">

	            ADMIN PANEL

	          </div>

	        </div>

	        <div class="col-md-7 col-sm-6 col-xs-6">

	          <div style="margin-top:21px; color:#fff; font-size:18px; float:left;" class="school-name"> 

			 

	            <strong>

	              CONVOCATION



	            </strong>

	          </div>

	        </div>



        <!-- END LOGO -->

        <!-- BEGIN RESPONSIVE MENU TOGGLER -->

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">

          

        </a>

        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN TOP NAVIGATION MENU -->

      

        <div class="top-menu">

          <!--  ======================== get notificatins from stat tabel for logged in user like student,teacher,gardian  ================ -->

          <?php

$user_type = $_SESSION['user_type']; 

$user_id = $_SESSION['myy'];

switch ($user_type) {

case '1':

//echo "select count(*) as `total_new_msg` from `grivance_sent_sms_admin` where `to_user_id` ='" . $user_id . "' and `is_new`= '1' "; exit;

$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `grivance_sent_sms_admin` where `to_user_id` ='" . $user_id . "' and `is_new`= '1' "));

break;

case '3':

$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `grivance_sent_sms_teacher` where `to_teacher_id` ='" . $user_id . "' and `is_new`= '1' "));

//$new_messages['total_new_msg'];

break;

case '4':

$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `grivance_sent_sms_student` where `student_id` ='" . $user_id . "' and `is_new`= '1' "));

break;

case '5':

$new_messages = mysql_fetch_assoc(mysql_query("select count(*) as `total_new_msg` from `grivance_sent_sms_guardian` where `guardian_id` ='" . $user_id . "' and `is_new`= '1' "));

break;

default:

break;

}

?>

          <!--  ======================== END get notificatins from stat tabel for logged in user like student,teacher,gardian  ============= -->

          <ul class="nav navbar-nav pull-right">


           

            <!-- END TODO DROPDOWN -->

            <!-- BEGIN USER LOGIN DROPDOWN -->

            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
<?php
$user_profile=mysql_fetch_assoc(mysql_query("SELECT * FROM `convocation_admin` where id='".$_SESSION['admin_id']."'"));
$profile_image=$user_profile['profile_img'];

				   
				   ?>
            <li class="dropdown dropdown-user">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                <img alt="Profile Image" class="img-circle" src="<?php echo !empty($profile_image)?'upload/documents/'.$profile_image:'no_image.png' ?>"/>

                <span class="username username-hide-on-mobile">

                  <!-- Get the current User type -->

                  <?php

/* =======================Query to get the loggedin user type================================ */

$sql_user_type = mysql_query("SELECT * FROM  `grivance_admin` WHERE username = '" . $_SESSION['username'] . "'");

$row_user_type = mysql_fetch_array($sql_user_type);

/* =======================Query to get the loggedin user type================================ */

if ($row_user_type['is_admin'] == 1) {

$header_name = 'Admin';

} elseif ($row_user_type['is_admin'] == 0) {

$header_name = 'Admin';

} elseif ($row_user_type['is_admin'] == 2) {

$admin_id = $row_user_type['id'];

$sql_insti_name = mysql_query("SELECT * FROM  `exammanage_institution` WHERE admin_id = '" . $admin_id . "'");

$row_insti_name = mysql_fetch_array($sql_insti_name);

$header_name = $row_insti_name['name'];

} elseif ($row_user_type['is_admin'] == 3) {

$admin_id = $row_user_type['id'];

$sql_teac_name = mysql_query("SELECT * FROM  `exammanage_teachers` WHERE user_id = '" . $admin_id . "'");

$row_teac_name = mysql_fetch_array($sql_teac_name);

$header_name = $row_teac_name['fname'] . ' ' . $row_teac_name['mname'] . ' ' . $row_teac_name['lname'];

}

?>

                  Welcome, &nbsp;

                  <?php echo $header_name; ?>

                  <?php if ($_SESSION['username'] != '') { ?>	(

                  <?php echo $_SESSION['username']; ?>)

                  <?php } ?>

                </span>

                <i class="fa fa-angle-down">

                </i>

              </a>

              <ul class="dropdown-menu dropdown-menu-default">

                <li>

                  <a href="changeusername.php">

                    <i class="icon-user">

                    </i> Change Username 

                  </a>

                </li>

                <li>

                  <a href="changeemail.php">

                    <i class="icon-envelope-open">

                    </i> Change Email 

                  </a>

                </li>

                <li>

                  <a href="changepassword.php">

                    <i class="icon-key">

                    </i> Change Password 

                  </a>

                </li>

				<li class="divider" style="background:#f1f3f6; height:1px; margin:9px 0; overflow:hidden"></li>

						<li>

							<a href="login_lock.php">

							<i class="icon-lock"></i> Lock Screen </a>

						</li>

                <li>

                  <a href="logout.php">

                    <i class="icon-power">

                    </i> Log Out 

                  </a>

                </li>

              </ul>

            </li>

            <!-- END USER LOGIN DROPDOWN -->

            <!-- BEGIN QUICK SIDEBAR TOGGLER -->

            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

            <li class="dropdown dropdown-quick-sidebar-toggler">

              <!--<a href="logout.php" class="dropdown-toggle">

<i class="icon-logout"></i>

</a>-->

            </li>

            <!-- END QUICK SIDEBAR TOGGLER -->

          </ul>

        </div>



        <!-- END TOP NAVIGATION MENU -->

      </div>

      <!-- END HEADER INNER -->

    </div>

    <!-- END HEADER -->

<div class="clearfix"></div>