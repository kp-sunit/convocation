<?php
   ob_start(); 

   $pagename1 = end(explode('/', $_SERVER['REQUEST_URI']));

   $pagename2 = explode('.', $pagename1);

   $pagename = $pagename2[0];

   $privilegeSettings = mysql_fetch_array(mysql_query("Select * from `filmfestival_admin` where `id`='" . $_SESSION['admin_id'] . "'"));

   $innerPrivileges = explode(',',$privilegeSettings['privileges']);

   $isAdmin = $privilegeSettings['is_admin'];



   ?>
<div class="page-sidebar-wrapper">
   <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
   <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
   <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
      <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
      <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
      <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
      <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
      <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
      <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="position:relative;">
         <?php /*?> <img src="<?php //echo $image_link;?>logo.png" style="position:absolute; max-width:150px; max-height:150px; top:-41px; left:18px; z-index:1111111111111111;" /><?php */?>
         <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
         <li class="sidebar-toggler-wrapper" style="display:none">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler">
            </div>
            <!-- END SIDEBAR TOGGLER BUTTON -->
         </li>
         <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
         <li class="sidebar-search-wrapper">
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <form class="sidebar-search " action="extra_search.html" method="POST">
               <!--  <strong style="color:#FFFFFF; font-size:18px">ADMIN PANEL</strong> --> <a href="javascript:;" class="remove">
               <i class="icon-close"></i>
               </a>
               <!-- <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search...">
                  
                  <span class="input-group-btn">
                  
                      <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                  
                  </span>
                  
                  </div>-->
            </form>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
         </li>
         <li class="start" style="margin-top:50px;">
            <a href="dashboard_admin.php" style=" background:#d64635 !important">
               <span class="title"><strong style="color:#FFFFFF; font-size:18px;">ADMIN PANEL</strong></span>
               <!--<span class="arrow "></span>-->
            </a>
         </li>
         <li class="start">
            <a href="dashboard_admin.php">
               <i class="icon-home"></i>
               <span class="title">Dashboard</span>
               <!--<span class="arrow "></span>-->
            </a>
         </li>
         
              
               
        
         
         <li <?php if($pagename=='list_guests' || $pagename=='add_guest'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Guests</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_guests' || $pagename=='add_guest'){?> style="display:block;"<?php } ?>>
               <li><a href="list_guests.php">List Guests</a></li>
            </ul>
         </li>
         <li class="start">
            <a href="add_university.php">
               <i class="icon-home"></i>
               <span class="title">University</span>
               <!--<span class="arrow "></span>-->
            </a>
         </li>
          <li class="start">
            <a href="add_program.php">
               <i class="icon-home"></i>
               <span class="title">Program</span>
               <!--<span class="arrow "></span>-->
            </a>
         </li>
         <li class="start">
            <a href="add_apphome.php">
               <i class="icon-home"></i>
               <span class="title">App Home</span>
               <!--<span class="arrow "></span>-->
            </a>
         </li>
         <li <?php if($pagename=='add_slider' || $pagename=='list_slider_images'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Slider</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='add_slider' || $pagename=='list_slider_images'){?> style="display:block;"<?php } ?>>
               <li><a href="list_slider_images.php">List Sliders</a></li>
            </ul>
         </li>
         <li <?php if($pagename=='list_prev_convocations' || $pagename=='add_prev_convocation'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Convocations</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_prev_convocations' || $pagename=='add_prev_convocation'){?> style="display:block;"<?php } ?>>
               <li><a href="list_prev_convocations.php">List Prev Convocations</a></li>
            </ul>
         </li>
         <li <?php if($pagename=='list_degrees' || $pagename=='add_degree'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Degrees</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_degrees' || $pagename=='add_degree'){?> style="display:block;"<?php } ?>>
               <li><a href="list_degrees.php">List Degrees</a></li>
            </ul>
         </li>
         <li <?php if($pagename=='list_departments' || $pagename=='add_department'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Departments</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_departments' || $pagename=='add_department'){?> style="display:block;"<?php } ?>>
               <li><a href="list_departments.php">List Departments</a></li>
            </ul>
         </li>
         <li <?php if($pagename=="generate_id_card_manual" || $pagename=="export_filter_idcard" || $pagename=="export_filter_pdf2" || $pagename=="export_filter_pdf" || $pagename=="export_filter" || $pagename=='list_registrations' || $pagename=='add_registration'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Registrations</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=="generate_id_card_manual" || $pagename=="export_filter_idcard" || $pagename=="export_filter_pdf2" || $pagename=="export_filter_pdf" || $pagename=="export_filter" || $pagename=='list_registrations' || $pagename=='add_registration'){?> style="display:block;"<?php } ?>>
               <li><a href="list_registrations.php">List Registrations</a></li>
               <li><a href="export_filter.php">Export Registrations</a></li>
               <li><a href="export_filter_pdf.php">Download P.D.F(1)</a></li>
               <li><a href="export_filter_pdf2.php">Download P.D.F(2)</a></li>
               <li><a href="export_filter_idcard.php">Download ID Card</a></li>
               <li><a href="generate_id_card_manual.php">Download ID Card Manual</a></li>
            </ul>
         </li>
<!--         <li <?php if($pagename=='list_students'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Students</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_students'){?> style="display:block;"<?php } ?>>
               <li><a href="list_students.php">List Students</a></li>
            </ul>
         </li>-->
         <li <?php if($pagename=='add_cms' || $pagename=='list_cms'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing CMS</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='add_cms' || $pagename=='list_cms'){?> style="display:block;"<?php } ?>>
               <li><a href="list_cms.php">List CMS</a></li>
            </ul>
         </li>
         
         
         <li <?php if($pagename=='list_all_students' || $pagename=='list_cms'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-puzzle"></i>
            <span class="title">Managing Students</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=='list_all_students' || $pagename=='add_students' || $pagename=='import_students' || $pagename=='truncate_students'){?> style="display:block;"<?php } ?>>
               <li><a href="list_all_students.php">List All Students</a></li>
               <li><a href="add_students.php">Add Student</a></li>
               <li><a href="import_students.php">Import Student</a></li>
               <li><a href="truncate_students.php">Truncate Student DB</a></li>
            </ul>
         </li>         
         
        
         <li <?php if($pagename=="manage_convocation" || $pagename=="sitesettings" || $pagename=="changeprofileimg" || $pagename=='changeusername' || $pagename=='update_adminprofile' || $pagename=='changeemail' || $pagename=='changepassword' || $pagename=='update_instruction' || $pagename=='updateattendence'){?> class="open"<?php } ?>>
            <a href="javascript:;">
            <i class="icon-logout"></i>
            <span class="title">Settings</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu" <?php if($pagename=="manage_convocation" || $pagename=="sitesettings" || $pagename=="changeprofileimg" || $pagename=='changeusername' || $pagename=='update_adminprofile' ||  $pagename=='changeprofileimage'|| $pagename=='changeemail' || $pagename=='changepassword' || $pagename=='update_instruction' || $pagename=='updateattendence'){?> style="display:block;"<?php } ?>>
               <li>
                  <a href="changeusername.php">
                  <i class="icon-pencil"></i> Change Username </a>
               </li>
               <li>
                  <a href="manage_convocation.php">
                  <i class="icon-pencil"></i> Manage Convocation Fees </a>
               </li>
               <li>
                  <a href="sitesettings.php">
                  <i class="icon-pencil"></i> Site Settings </a>
               </li>
               <!-- <li>
                  <a href="update_adminprofile.php?action=edit&id=1">
                  
                  <i class="icon-pencil"></i> Update admin profile </a>
                  
                  </li>
                  
                  <li>
                  
                  <a href="update_instruction.php?id=1">
                  
                  <i class="icon-pencil"></i> Update Instruction </a>
                  
                  </li>--> 
               <li>
                  <a href="changeemail.php">
                  <i class="icon-pencil"></i> Change Email </a>
               </li>
               <li>
                  <a href="changeprofileimg.php">
                  <i class="icon-pencil"></i> Change Image </a>
               </li>
               <li>
                  <a href="changepassword.php">
                  <i class="icon-pencil"></i> Change Password </a>
               </li>
               <!--<li>
                  <a href="updateattendence.php">
                  
                  <i class="icon-pencil"></i> Update attendance </a>
                  
                  </li>-->
               <li>
                  <a href="logout.php">
                  <i class="icon-pencil"></i> Logout</a>
               </li>
            </ul>
         </li>
      </ul>
   </div>
</div>
<style>
   li > a > .badge {
   position: absolute;
   margin-top: 1px;
   right: 21px;
   display: inline;
   font-size: 11px;
   font-weight: 300;
   text-shadow: none;
   height: 18px;
   padding: 4px 6px 3px 6px;
   text-align: center;
   vertical-align: middle;
   -webkit-border-radius: 12px !important;
   -moz-border-radius: 12px !important;
   border-radius: 12px !important;
   /*	background:#d64635 !important;*/
   }
</style>

