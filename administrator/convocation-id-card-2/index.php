<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>ID Card</title>
<meta name="" content="">
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="page-wrap">
		<table class="id-table" cellpadding="15" cellspacing="0" width="100%">
			<tbody>
				<tr class="id-top">
					<td class="logo-area">
						<div class="top-image"><img src="images/university_logo.png" alt=""></div>
					</td>
					<td align="center" class="title-area">
						<div class="top-content">
							<image src="images/main.png" alt="" class="main-image"></image>
						</div>
					</td>
					<td class="logo-area" align="right">
						<div class="top-image"><img src="images/university_logo.png" alt=""></div>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<div class="student-image"><img src="images/pic.jpg" alt=""></div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table class="id-table center-table" cellpadding="15" cellspacing="0" width="70%">
							<tr class="table-row">
								<td class="title">Name:</td>
								<td class="desc" colspan="2">John Doe</td>
							</tr>
							<tr class="table-row">
								<td class="title">ID:</td>
								<td class="desc" colspan="2">JD-58963214</td>
							</tr>
							<tr class="table-row">
								<td class="title">Description:</td>
								<td class="desc" colspan="2">Lorem imsum dolor sit amter conseqr</td>
							</tr>
						</table>
					</td>
					
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>