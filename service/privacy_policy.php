<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Privacy Policy</title>
</head>

<body>
<h1>Privacy Policy</h1>
<p><strong>OVERVIEW</strong></p>
<p>This privacy policy sets out how Leading University Convocation 2019 uses and protects any information that you give Leading University Convocation &nbsp;when you use this app.</p>
<p>Leading University Convocatio&nbsp;is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this app, then you can be assured that it will only be used in accordance with this privacy statement.</p>
<p>&quot;We do not store credit card/finacial details nor do we share customer details with any 3rd parties&quot;</p>
<p>Leading University Convocation&nbsp;may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 1 January 2018.</p>
<p><strong>What we collect</strong></p>
<p>We may collect the following information:</p>
<ul>
  <li>Name and job title</li>
  <li>Contact information including email address</li>
  <li>Demographic information such as postcode</li>
  <li>Other information relevant to customer surveys and/or offers</li>
</ul>
<p><strong>What we do with the information we gather</strong></p>
<p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>
<ul>
  <li>Internal record keeping.</li>
  <li>We may use the information to improve our products and services.</li>
  <li>We may periodically send promotional emails about new things, other information which we think you may find interesting using the email address which you have provided.</li>
  <li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the app according to your interests.</li>
</ul>
<p><strong>Security</strong></p>
<p>We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
<p><strong>Links to other websites</strong></p>
<p>This app is not linked to any social website. we will not share your data with any social website.</p>
<p><strong>Controlling your personal information</strong></p>
<p>You may choose to restrict the collection or use of your personal information in the following ways:</p>
<ul>
  <li>Whenever you are asked to fill in a form on the app, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</li>
  <li>If you have previously agreed to us using your personal information , you may change your mind at any time by writing to or emailing us.</li>
  <li>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</li>
</ul>
<p>You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please email us sales [at] leadinguniversity [dot] com.<br />
  If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>
</body>
</html>
