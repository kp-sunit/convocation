<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require ('./vendor/autoload.php');
$app = new \Slim\Slim(array(
    'debug' => false
        ));

class AllCapsMiddleware extends \Slim\Middleware {

    public function call() {
        // Get reference to application
        $app = $this->app;

        $req = $app->request;

        //print_r($req);exit;
        // Run inner middleware and application
        $this->next->call();

        // Capitalize response body

        $res = $app->response;
        //$body = $res->getBody();
        if ($req->headers->get('Token') != "123456") {
            $res->setStatus(401);
            $res->setBody("{\"msg\":\"not authorised\"}");
        }
    }

}

$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);

$app->add($cors);



//$app->add(new \CorsSlim\CorsSlim());
//$app->add(new \AllCapsMiddleware());
//$app->response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
$app->response->headers->set('Content-Type', 'application/json');


$app->group('', function () use ($app) {
$app->get('/test', 'test');
$app->post('/listGuests', "listGuests");
$app->post('/listPreviousConvocations', "listPreviousConvocations");
//$app->post('/previousConvocationDetails', "previousConvocationDetails");
//$app->post('/register', "register");
$app->post('/getCMS', "getCMS");
$app->post('/getContents', "getContents");
$app->post('/getStudentDetailsByID', "getStudentDetailsByID");
$app->post('/getStudentDetailsByPVC', "getStudentDetailsByPVC");

$app->post('/postRegistration', "postRegistration");
$app->post('/updateProfilePhoto', "updateProfilePhoto");
$app->post('/updateReceiptPhoto', "updateReceiptPhoto");
$app->post('/updateProfilePhotoIOS', "updateProfilePhotoIOS");
$app->post('/updateReceiptPhotoIOS', "updateReceiptPhotoIOS");
$app->post('/getAllDegree', "getAllDegree");
$app->post('/getAllDepertment', "getAllDepertment");
$app->post('/getAllFaculty', "getAllFaculty");
$app->post('/getPrice', "getPrice");
$app->get('/validatePayment', "validatePayment");
$app->get('/confirmPayment', "confirmPayment");

});
